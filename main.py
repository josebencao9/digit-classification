import PIL
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image, ImageOps
import tensorflow as tf
from keras.layers import Rescaling, Conv2D, Flatten, Dropout, Activation, MaxPooling2D, Dense


def png_to_greyscale(filename):
    img = Image.open(filename).convert('L')
    im2 = img.resize((28, 28))
    PIL.ImageOps.invert(im2).save(str('final' + filename))


def greyscale_to_matrix(filename):
    imageAnalyze = Image.open(str('final' + filename))

    image_matrix = np.asarray(imageAnalyze)
    return np.array([image_matrix])


data = tf.keras.datasets.mnist
(train_images, train_labels), (test_images, test_labels) = data.load_data()

unique, counts = np.unique(train_labels, return_counts=True)
print(dict(zip(unique, counts)))

class_names = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
model = tf.keras.Sequential([
    Rescaling(1. / 255, input_shape=(28, 28,1)),
    Conv2D(32, 3, padding='same', activation='relu'),
    MaxPooling2D(),
    Conv2D(64, 3, padding='same', activation='relu'),
    MaxPooling2D(),
    Conv2D(128, 3, padding='same', activation='relu'),
    MaxPooling2D(),
    Flatten(),
    Dense(256, activation='relu'),
    Dropout(0.5),
    Dense(10, activation='softmax')])

model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])

model.fit(train_images, train_labels, epochs=2)

model.save_weights('./model')


teste_loss, test_acc = model.evaluate(test_images, test_labels)

png_to_greyscale('image.png')
img = greyscale_to_matrix('image.png')
prediction = model.predict(img)
plt.imshow(img[0])
plt.show()

for i in range(10):
    print((class_names[i], float(prediction[0][i] * 100)))
